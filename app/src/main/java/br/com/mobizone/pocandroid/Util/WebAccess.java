package br.com.mobizone.pocandroid.Util;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Intermat on 16/01/17.
 */

public class WebAccess {


    public final String URL_SERVER = "https://api.themoviedb.org/3/movie/popular";

    public final String API_KEY = "04817a57f3bfb81719b82e89126b3490";

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    public String getMovieList() throws IOException {

        String urlMethod = String.format("%s?api_key=%s&language=pt-BR&page=1", URL_SERVER, API_KEY);

        Request request = new Request.Builder()
                .url(urlMethod)
                .build();


        Response response = client.newCall(request).execute();
        return response.body().string();

    }


    public String getToken() throws IOException {

        String urlMethod = String.format("https://api.themoviedb.org/3/authentication/token/new?api_key=%s", API_KEY);

        Request request = new Request.Builder()
                .url(urlMethod)
                .build();


        String retorno = "";

        Response response = client.newCall(request).execute();

        try{
            JSONObject objJSON = new JSONObject(response.body().string());
            retorno = objJSON.getString("request_token");
        }catch (JSONException ex){
            Log.v("DEMO", ex.getMessage());
        }

        return retorno;

    }


    public String getSession() throws IOException {

        String requestToken = getToken();

        String urlMethod = String.format("https://api.themoviedb.org/3/authentication/session/new?api_key=%s&request_token=%s", API_KEY, requestToken);

        Request request = new Request.Builder()
                .url(urlMethod)
                .build();

        String retorno = "";

        Response response = client.newCall(request).execute();

        try{
            JSONObject objJSON = new JSONObject(response.body().string());
            retorno = objJSON.getString("session_id");
        }catch (JSONException ex){
            Log.v("DEMO", ex.getMessage());
        }

        return retorno;

    }


    public void setRate(int movieID) throws IOException {

        String urlMethod = String.format("https://api.themoviedb.org/3/movie/%s/rating?api_key=%s&session_id=%s",
                movieID, API_KEY, getSession());


        JSONObject jsonConfig = new JSONObject();

        try {
            jsonConfig.put("value", 8.5);

        } catch (JSONException ss) {
            Log.v("DEMO", ss.getMessage());
        }


        String json = jsonConfig.toString();

        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(urlMethod)
                .post(body)
                .build();

        Response response = client.newCall(request).execute();

    }

}
