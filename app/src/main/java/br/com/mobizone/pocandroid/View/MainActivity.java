package br.com.mobizone.pocandroid.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import br.com.mobizone.pocandroid.Model.Movie;
import br.com.mobizone.pocandroid.Controller.MovieController;
import br.com.mobizone.pocandroid.R;
import br.com.mobizone.pocandroid.Util.MovieAdapter;
import br.com.mobizone.pocandroid.Util.WebAccess;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute();

        listView = (ListView) findViewById(R.id.listViewMovie);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Movie objMovie = (Movie) listView.getAdapter().getItem(i);

                Intent it = new Intent(MainActivity.this, br.com.mobizone.pocandroid.View.RateMovie.class);
                it.putExtra("title", objMovie.title );
                it.putExtra("overview", objMovie.overview );
                it.putExtra("id", objMovie.id);
                it.putExtra("poster", objMovie.poster_path);

                startActivity(it);
            }
        });




    }

    private class DownloadWebPageTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {



            WebAccess webAccess = new WebAccess();
            String retorno = "";
            try {

                retorno = webAccess.getMovieList();

            } catch (Exception ex) {
                Log.v("DEMO", ex.getMessage());
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(String result) {

            try {

                JSONObject objJson = new JSONObject(result);
                JSONArray arrJson = objJson.getJSONArray("results");

                MovieController controller = new MovieController();

                controller.addMovie(arrJson);

                MovieAdapter adapter = new MovieAdapter(MainActivity.this, controller.getListMovie());
                listView.setAdapter(adapter);

            }
            catch (Exception ex){
                Log.v("DEMO", ex.getMessage());
            }

        }
    }


}
