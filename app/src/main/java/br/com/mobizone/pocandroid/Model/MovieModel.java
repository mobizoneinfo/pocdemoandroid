package br.com.mobizone.pocandroid.Model;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Intermat on 16/01/17.
 */

public class MovieModel {



    List<Movie> listMovie;

    public MovieModel() {

        listMovie = new ArrayList<Movie>();

    }

    public void addMovie(Movie movie){
        listMovie.add(movie);
    }



    public void removeMovie(Movie movie){

        listMovie.remove(movie);
    }

    public List<Movie> getListMovie() {
        return listMovie;
    }
}
