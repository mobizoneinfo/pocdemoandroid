package br.com.mobizone.pocandroid.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import br.com.mobizone.pocandroid.Controller.MovieController;
import br.com.mobizone.pocandroid.R;
import br.com.mobizone.pocandroid.Util.ImageDownloaderTask;
import br.com.mobizone.pocandroid.Util.WebAccess;

public class RateMovie extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_movie);


        TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText(getIntent().getStringExtra("title"));


        TextView txtOverview = (TextView) findViewById(R.id.txtOverview);
        txtOverview.setText(getIntent().getStringExtra("overview"));

        ImageView imgMovie = (ImageView) findViewById(R.id.imgVideo);


        ImageDownloaderTask download =   new ImageDownloaderTask(imgMovie);

        String url_Poster = String.format("%s%s", "https://image.tmdb.org/t/p/w185", getIntent().getStringExtra("poster"));

        download.execute(url_Poster);


        Button btnAvaliar = (Button)findViewById(R.id.btnAvaliar);
        btnAvaliar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                MovieController controller = new MovieController();
                controller.addMovieRate(getIntent().getIntExtra("id", 0));

                Toast.makeText(RateMovie.this, "Avaliação efetuada", Toast.LENGTH_LONG).show();;

            }
        });


        Button btnRetiar = (Button)findViewById(R.id.btnRetirar);
        btnRetiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(RateMovie.this, "Avaliação removida", Toast.LENGTH_LONG).show();;

            }
        });

    }
}
