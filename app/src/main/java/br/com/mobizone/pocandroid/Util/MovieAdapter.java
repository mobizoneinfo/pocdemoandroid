package br.com.mobizone.pocandroid.Util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.mobizone.pocandroid.Model.Movie;
import br.com.mobizone.pocandroid.R;
import br.com.mobizone.pocandroid.Util.ImageDownloaderTask;

/**
 * Created by Intermat on 16/01/17.
 */

public class MovieAdapter  extends BaseAdapter{

    List<Movie> movieList;
    Context context;

    public MovieAdapter(Context context, List<Movie> list){

        this.context = context;
        movieList = list;

    }

    @Override
    public int getCount() {
        return movieList.size();
    }

    @Override
    public Object getItem(int i) {
        return movieList.get(i);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {


        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_view_itens, null);

            holder = new ViewHolder();

            holder.title  = (TextView) convertView.findViewById(R.id.textTitle);
            holder.detail  = (TextView) convertView.findViewById(R.id.textDetail);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);

            convertView.setTag(holder);

        }else {

            holder = (ViewHolder) convertView.getTag();
        }

        Movie objMovie =  movieList.get(position);

        holder.title.setText(objMovie.title);
        holder.detail.setText(objMovie.overview);

        if (holder.imageView != null) {
            new ImageDownloaderTask(holder.imageView).execute(String.format("%s/%s", "https://image.tmdb.org/t/p/w92/", objMovie.poster_path));
        }


        return convertView;

    }


    static class ViewHolder {
        TextView title;
        TextView detail;
        ImageView imageView;
    }
}

