package br.com.mobizone.pocandroid.Controller;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import br.com.mobizone.pocandroid.Model.Movie;
import br.com.mobizone.pocandroid.Model.MovieModel;
import br.com.mobizone.pocandroid.Util.MovieAdapter;
import br.com.mobizone.pocandroid.Util.WebAccess;
import br.com.mobizone.pocandroid.View.MainActivity;

/**
 * Created by Intermat on 16/01/17.
 */

public class MovieController {

    MovieModel model;

    public MovieController() {

        model = new MovieModel();
    }

    public void addMovie(Movie movie) {
        model.addMovie(movie);
    }

    public void removeMovie(Movie movie) {
        model.removeMovie(movie);
    }

    public void addMovie(JSONArray movies) {

        Gson gson = new Gson();
        Movie[] myMovies = gson.fromJson(movies.toString(), Movie[].class);

        for (int i = 0; i < myMovies.length; i++) {
            model.addMovie(myMovies[i]);
        }

    }

    public List<Movie> getListMovie() {
        return model.getListMovie();
    }


    public void addMovieRate(Integer rate) {

        RateTask rateTask = new RateTask();
        rateTask.execute(rate);

    }

    public void removeMovieRate(Integer rate) {

        //Not implmented

    }


    private class RateTask extends AsyncTask<Integer, Void, String> {

        @Override
        protected String doInBackground(Integer... params) {

            WebAccess webAccess = new WebAccess();
            try {

                webAccess.setRate(params[0]);

            } catch (Exception ex) {
                Log.v("DEMO", ex.getMessage());
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {

            Log.v("DEMO", "ssssss");

        }
    }


}
